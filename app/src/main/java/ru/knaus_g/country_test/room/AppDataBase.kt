package ru.knaus_g.country_test.room

import androidx.room.Database
import androidx.room.RoomDatabase
import ru.knaus_g.country_test.models.Country

@Database(entities = [Country::class], version = 1, exportSchema = false)
abstract class AppDataBase : RoomDatabase() {
    abstract fun countryDao(): CountryDao
}