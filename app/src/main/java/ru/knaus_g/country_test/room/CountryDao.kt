package ru.knaus_g.country_test.room

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ru.knaus_g.country_test.models.Country

@Dao
interface CountryDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addCountry(array_country: List<Country>)


    @Query("SELECT * FROM country ORDER BY name")
    fun getAllCountry(): LiveData<List<Country>>


    @Query("SELECT COUNT(*) FROM country")
    suspend fun getCount(): Int
}