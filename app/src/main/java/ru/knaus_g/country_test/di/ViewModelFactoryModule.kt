package ru.knaus_g.country_test.di

import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import ru.knaus_g.country_test.viewmodel.ViewModelFactory

@Module
abstract class ViewModelFactoryModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory):ViewModelProvider.Factory

}