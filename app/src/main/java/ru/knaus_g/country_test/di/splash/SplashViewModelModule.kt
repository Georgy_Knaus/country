package ru.knaus_g.country_test.di.splash

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import ru.knaus_g.country_test.di.ViewModelKey
import ru.knaus_g.country_test.ui.splash.SplashViewModel

@Module
abstract class SplashViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel::class)
    internal abstract fun bindViewModel(model: SplashViewModel): ViewModel
}