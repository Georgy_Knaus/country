package ru.knaus_g.country_test.di

import android.app.Application
import android.content.Context
import androidx.room.Room
import dagger.Binds
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ru.knaus_g.country_test.network.CountryApi
import ru.knaus_g.country_test.repositories.CountryRepository
import ru.knaus_g.country_test.repositories.ICountryRepository
import ru.knaus_g.country_test.room.AppDataBase
import javax.inject.Singleton

@Module(includes = [AppModule.BindsModule::class])
class AppModule {

    @Singleton
    @Provides
    fun providesDataBase(application: Application) =
        Room.databaseBuilder(application.applicationContext, AppDataBase::class.java, "myDB")
            .build()

    @Singleton
    @Provides
    fun providesRadioDao(database: AppDataBase) = database.countryDao()


    @Singleton
    @Provides
    fun provideRetrofitInstance() = Retrofit.Builder()
        .baseUrl("https://restcountries.eu/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(CountryApi::class.java)


    @Module
    interface BindsModule {

        @Binds
        @Singleton
        fun bindCountryRepository(countryRepository: CountryRepository): ICountryRepository
    }


    @Singleton
    @Provides
    fun provideContext(application: Application): Context {
        return application.applicationContext
    }


}