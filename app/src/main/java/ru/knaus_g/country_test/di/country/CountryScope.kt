package ru.knaus_g.country_test.di.country

import javax.inject.Scope

@MustBeDocumented
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class CountryScope