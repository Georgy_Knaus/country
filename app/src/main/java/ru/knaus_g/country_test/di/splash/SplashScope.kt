package ru.knaus_g.country_test.di.splash

import javax.inject.Scope

@MustBeDocumented
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class SplashScope