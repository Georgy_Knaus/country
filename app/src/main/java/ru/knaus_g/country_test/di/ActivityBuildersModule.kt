package ru.knaus_g.country_test.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ru.knaus_g.country_test.di.country.CountryScope
import ru.knaus_g.country_test.di.country.CountryViewModelModule
import ru.knaus_g.country_test.di.main.MainScope
import ru.knaus_g.country_test.di.main.MainViewModelModule
import ru.knaus_g.country_test.di.splash.SplashScope
import ru.knaus_g.country_test.di.splash.SplashViewModelModule
import ru.knaus_g.country_test.ui.country.CountryActivity
import ru.knaus_g.country_test.ui.main.MainActivity
import ru.knaus_g.country_test.ui.splash.SplashActivity

@Module
abstract class ActivityBuildersModule {

    @MainScope
    @ContributesAndroidInjector(
        modules = [MainViewModelModule::class]
    )
    abstract fun contributeMainActivity(): MainActivity



    @CountryScope
    @ContributesAndroidInjector(
        modules = [CountryViewModelModule::class]
    )
    abstract fun contributeCountryActivity(): CountryActivity



    @SplashScope
    @ContributesAndroidInjector(
        modules = [SplashViewModelModule::class]
    )
    abstract fun contributeSplashActivity(): SplashActivity
}