package ru.knaus_g.country_test.di.main

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import ru.knaus_g.country_test.di.ViewModelKey
import ru.knaus_g.country_test.ui.main.MainViewModel
import ru.knaus_g.country_test.ui.splash.SplashViewModel

@Module
abstract class MainViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    internal abstract fun bindViewModel(model: MainViewModel): ViewModel

}