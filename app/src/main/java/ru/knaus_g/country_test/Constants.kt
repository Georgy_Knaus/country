package ru.knaus_g.country_test

class Constants {

    companion object{
        val COUNTRY = "Country"
        val COUNTRY_NAME = "country_name"
        val COUNTRY_FLAG = "country_flag"
        val COUNTRY_CAPITAL = "country_capital"
        val COUNTRY_CURRENCY = "country_currency"
    }


}
