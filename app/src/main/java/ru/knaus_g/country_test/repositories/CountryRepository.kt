package ru.knaus_g.country_test.repositories


import android.content.Context
import android.net.ConnectivityManager
import androidx.core.content.ContextCompat.getSystemService
import androidx.lifecycle.LiveData
import ru.knaus_g.country_test.models.Country
import ru.knaus_g.country_test.network.CountryApi
import ru.knaus_g.country_test.room.CountryDao
import javax.inject.Inject


class CountryRepository @Inject constructor(
    private val countryApi: CountryApi,
    private val countryDao: CountryDao,
    private val context: Context
) : ICountryRepository {


    fun  getLiveDataCountry(): LiveData<List<Country>>{
        return countryDao.getAllCountry()
    }


    override suspend fun loadCountry() {

        val count = countryDao.getCount()

        if (count == 0) {
            if (isNetworkConnected(context)) {

                val array_country = countryApi.getCountry().map {
                        Country(
                            it.name ?: "",
                            it.flag ?: "",
                            it.currencies?.get(0)?.name ?: "",
                            it.capital ?: ""
                        )
                    }
                array_country.let {
                    countryDao.addCountry(array_country)
                }
            } else {
                throw NotInternetException()
            }
        }
    }

    private fun isNetworkConnected(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return cm.activeNetworkInfo != null && cm.activeNetworkInfo.isConnected

    }




}

class NotInternetException : Exception()

interface ICountryRepository {
    suspend fun loadCountry()

}