package ru.knaus_g.country_test.network


import retrofit2.http.GET
import ru.knaus_g.country_test.network.parse.CountryResponce

interface CountryApi {

    @GET("rest/v2/all")
    suspend fun getCountry(): List<CountryResponce>

}