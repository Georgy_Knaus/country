package ru.knaus_g.country_test.ui.country

import android.os.Bundle
import android.view.MenuItem
import com.bumptech.glide.Glide
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_country.*
import ru.knaus_g.country_test.Constants
import ru.knaus_g.country_test.R

class CountryActivity : DaggerAppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_country)

        init()

        country_name.text = getCountry()

        Glide
            .with(this)
            .load(intent.getStringExtra(Constants.COUNTRY_FLAG))
            .into(country_flag)

        country_capital.text =
            StringBuilder(getString(R.string.capital)).append(intent.getStringExtra(Constants.COUNTRY_CAPITAL))

        country_currency.text =
            StringBuilder(getString(R.string.currency)).append(intent.getStringExtra(Constants.COUNTRY_CURRENCY))
    }


    private fun init() {
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = getCountry()
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }

        return true
    }

    private fun getCountry(): String = intent.getStringExtra(Constants.COUNTRY_NAME)!!
}