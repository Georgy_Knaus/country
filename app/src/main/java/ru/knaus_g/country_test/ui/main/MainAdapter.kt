package ru.knaus_g.country_test.ui.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.main_adapter_country.view.*
import ru.knaus_g.country_test.util.GlideApp
import ru.knaus_g.country_test.R
import ru.knaus_g.country_test.models.Country

class MainAdapter(private val interaction: ViewHolder.Interaction? = null) :
    RecyclerView.Adapter<MainAdapter.ViewHolder>() {

    var array_country = ArrayList<Country>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
       val vh =  ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.main_adapter_country,
                parent,
                false
            )
        )

        vh.itemView.setOnClickListener {
            interaction?.onCountryClick(array_country[vh.adapterPosition])
        }

        return vh
    }

    override fun getItemCount(): Int {
        return array_country.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(array_country[position])
    }


    fun submitList(new_array: List<Country>) {
        array_country.clear()
        array_country.addAll(new_array)
        notifyDataSetChanged()
    }

    class ViewHolder constructor(
        itemView: View
    ) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: Country) {

            GlideApp.with(itemView)
                .load(item.flag)
                .into(itemView.country_flag)


            itemView.country_name.text = item.name

        }


        interface Interaction {
            fun onCountryClick(item: Country)
        }
    }


}