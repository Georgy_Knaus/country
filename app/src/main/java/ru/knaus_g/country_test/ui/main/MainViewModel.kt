package ru.knaus_g.country_test.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import ru.knaus_g.country_test.models.Country
import ru.knaus_g.country_test.network.CountryApi
import ru.knaus_g.country_test.repositories.CountryRepository
import ru.knaus_g.country_test.room.CountryDao
import javax.inject.Inject

class MainViewModel @Inject constructor(private val repository: CountryRepository) : ViewModel(){


    fun getLiveDataCountry(): LiveData<List<Country>>{
       return repository.getLiveDataCountry()
    }
}