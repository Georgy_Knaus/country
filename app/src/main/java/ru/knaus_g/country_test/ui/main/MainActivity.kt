package ru.knaus_g.country_test.ui.main

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import ru.knaus_g.country_test.Constants
import ru.knaus_g.country_test.R
import ru.knaus_g.country_test.models.Country
import ru.knaus_g.country_test.ui.country.CountryActivity
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity(), MainAdapter.ViewHolder.Interaction {

    private lateinit var adapter: MainAdapter

    @Inject
    lateinit var factory: ViewModelProvider.Factory

    private val model: MainViewModel by lazy {
        ViewModelProviders.of(this, factory)[MainViewModel::class.java]
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()

        model.getLiveDataCountry().observe(this, Observer {
            adapter.submitList(it)
        })
    }


    private fun init() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = Constants.COUNTRY

        recycler_view_country.layoutManager = GridLayoutManager(this, 2)
        adapter = MainAdapter(this@MainActivity)
        recycler_view_country.adapter = adapter
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return true
    }

    override fun onCountryClick(item: Country) {
        val intent = Intent(this, CountryActivity::class.java)
        intent.putExtra(Constants.COUNTRY_NAME, item.name)
        intent.putExtra(Constants.COUNTRY_FLAG, item.flag)
        intent.putExtra(Constants.COUNTRY_CAPITAL, item.capital)
        intent.putExtra(Constants.COUNTRY_CURRENCY, item.currency)
        startActivity(intent)
    }
}
