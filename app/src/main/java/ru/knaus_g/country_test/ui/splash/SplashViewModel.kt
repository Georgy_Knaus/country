package ru.knaus_g.country_test.ui.splash

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import ru.knaus_g.country_test.network.Result
import ru.knaus_g.country_test.repositories.CountryRepository
import java.lang.Exception
import javax.inject.Inject

class SplashViewModel @Inject constructor(
    private val repository: CountryRepository
) : ViewModel() {


    private var job: Job? = null

    private val _loadDataEvent = MutableLiveData<Result<Unit>>()
    val loadDataEvent: LiveData<Result<Unit>> = _loadDataEvent

    init {
        loadData()
    }


    fun loadData() {
        if (job == null || job?.isActive == false) {
            job = viewModelScope.launch(IO) {
                _loadDataEvent.postValue(Result.Loading())
                try {
                    repository.loadCountry()
                    _loadDataEvent.postValue(Result.Success(Unit))
                } catch (e: Exception) {
                    _loadDataEvent.postValue(Result.Error(e))
                }
            }
        }
    }

}