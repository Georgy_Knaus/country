package ru.knaus_g.country_test.ui.splash

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import dagger.android.support.DaggerAppCompatActivity
import ru.knaus_g.country_test.R
import ru.knaus_g.country_test.network.Result
import ru.knaus_g.country_test.repositories.NotInternetException
import ru.knaus_g.country_test.ui.main.MainActivity
import javax.inject.Inject

class SplashActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var factory: ViewModelProvider.Factory

    val model: SplashViewModel by lazy {
        ViewModelProviders.of(this, factory)[SplashViewModel::class.java]
    }

    private var alertDialog: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        model.loadDataEvent.observe(this, Observer {
            when (it) {
                is Result.Success -> {
                    val intent = Intent(this@SplashActivity, MainActivity::class.java)
                    startActivity(intent)
                    finish()
                }
                is Result.Error -> {
                    val message =
                        if (it.exception is NotInternetException) {
                            R.string.no_internet_connection
                        } else {
                            R.string.problem_with_server
                        }

                    val alertDialog = displayErrorDialog(getString(message))
                    this.alertDialog = alertDialog
                    alertDialog.show()

                }
                is Result.Loading -> {
                }
            }
        })


    }


    private fun displayErrorDialog(message: String) =
        AlertDialog.Builder(this)
            .setTitle(R.string.error)
            .setMessage(message)
            .setCancelable(false)
            .setPositiveButton(R.string.retry) { dialogInterface: DialogInterface, i: Int ->
                model.loadData()
            }
            .setNegativeButton(R.string.exit) { dialogInterface: DialogInterface, i: Int ->
                finish()
            }
            .create()

    override fun onDestroy() {
        super.onDestroy()
        alertDialog?.dismiss()

    }
}