package ru.knaus_g.country_test.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "country")
data class Country(
    @PrimaryKey
    val name: String = "",
    val flag: String = "",
    val currency: String = "",
    val capital: String = ""
)