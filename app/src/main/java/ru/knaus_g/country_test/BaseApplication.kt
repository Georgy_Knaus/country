package ru.knaus_g.country_test

import com.facebook.stetho.Stetho
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import ru.knaus_g.country_test.di.DaggerAppComponent

class BaseApplication :DaggerApplication(){


    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.builder().application(this).build()
    }


    override fun onCreate() {
        super.onCreate()

        // Create an InitializerBuilder

        // Create an InitializerBuilder
        val initializerBuilder: Stetho.InitializerBuilder = Stetho.newInitializerBuilder(this)

        // Enable Chrome DevTools

        // Enable Chrome DevTools
        initializerBuilder.enableWebKitInspector(
            Stetho.defaultInspectorModulesProvider(this)
        )

        // Enable command line interface

        // Enable command line interface
        initializerBuilder.enableDumpapp(
            Stetho.defaultDumperPluginsProvider(this)
        )

        // Use the InitializerBuilder to generate an Initializer

        // Use the InitializerBuilder to generate an Initializer
        val initializer: Stetho.Initializer = initializerBuilder.build()

        // Initialize Stetho with the Initializer

        // Initialize Stetho with the Initializer
        Stetho.initialize(initializer)
    }
}